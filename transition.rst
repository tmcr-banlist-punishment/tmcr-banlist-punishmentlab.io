Transition
==========

A transition is the representation a warp from an aera to a point in the game.

In ROM, this object is represented as a 20 (0x14)byte long structure, described as follows:

+---------------+----------+----------------------+----------------------+--------------------+--------------------+-----------------+----------------+----------------+-------------+--------------------+------------------+--------------+
| Warp Type ID  | Subtype? | StartPoint XPosition | StartPoint YPosition | EndPoint XPosition | EndPoint YPosition |  Warp Shape ID  | EndPos Area ID | EndPos Room ID | Exit Height | Transition Type ID | Facing Direction |     ????     |
+===============+==========+======================+======================+====================+====================+=================+================+================+=============+====================+==================+==============+
|      0x00     |   0x01   |       0x02-0x03      |       0x04-0x05      |      0x06-0x07     |      0x08-0x09     |       0x0A      |      0x0B      |       0x0C     |     0x0D    |        0x0E        |        0x0F      |  0x10 - 0x13 |
+---------------+----------+----------------------+----------------------+--------------------+--------------------+-----------------+----------------+----------------+-------------+--------------------+------------------+--------------+

In some cases, the startPoint positions data aren't written/read:

  - When using a Aera Warp
  - When using this with a :doc:`Manager6 <manager/Manager6>`

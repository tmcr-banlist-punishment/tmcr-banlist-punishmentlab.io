Manager 1E
==========

This :doc:`Manager </manager>` sets a flag whenever the player is in a specified region. It can also reset that same flag whenever the player is outside said region, if **Reset Flag** is nonzero.


+------------------------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------+
|            Object Header           |                                                                                                                                                         |
+-------------------+------+---------+------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+------------+------------+-----------+-----------+
|                   | Type | Subtype | Reset Flag |        | Action |      |                 |           |           | Width     | Height    | X Position | Y Position |           | Flag ID   |
+===================+======+=========+============+========+========+======+=================+===========+===========+===========+===========+============+============+===========+===========+
|                   | Loaded From ROM                      |               | Loaded From ROM |           |                                     Loaded From ROM                                 |
+-------------------+------+---------+------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+------------+------------+-----------+-----------+
|    0x00 - 0x07    | 0x08 |  0x09   |  0x0A      |  0x0B  |  0x0C  | 0x0D | 0x0E            | 0x0F-0x2F | 0x30-0x33 | 0x34-0x35 | 0x36-0x37 | 0x38-0x39  | 0x3a-0x3b  | 0x3c-0x3d | 0x3e-0x3f |
+-------------------+------+---------+------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+------------+------------+-----------+-----------+
| next/prev pointer | 0x09 |  0x1E   |            |        |        |      |                 |           |           |           |           |            |            |           |           |
| (used internally) |      |         |            |        |        |      |                 |           |           |           |           |            |            |           |           |
+-------------------+------+---------+------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+------------+------------+-----------+-----------+

In the ROM, **X Position** and **Y Position** refer to the top left corner of the region. However, during initialization, these are adjusted to the center of the region instead, and **Width** and **Height** are halved to be used as a radius instead. Thus, and odd **Width** or **Height** will be exactly the same as if it was the even number directly below it.

Manager B
=========

This :doc:`Manager </manager>` is used to create encounters, i.e. load enemies and trigger something when they are defeated.


Manager B exists in two forms, though only the Controller (Form 0) should be loaded from ROM.

Controller
----------

The Controller controls a fight. It can wait until a flag is set to start the fight. It also has a completion flag, that is set after the fight is completed and prevents it from being started if set before the manager is initialized.

It can also change the music to be played during a fight, if an inhibitor flag exists and Keep Music is 0. If it does so, it sets the music during the fight to the room var's field at offset 0x09 if that is non-zero, and 0x33 otherwise.

+-------------------+------+---------+------+-----------------------------+-------------------+------+-------------+-----------+---------------+-----------+--------------+-----------+----------------+-------------------------------+
|                   | Type | Subtype | Form | Object List Prop            | Action            |      | Counter     |           | Music restore |           |  Keep Music  |           | Inhibitor Flag | Completion Flag               |
+===================+======+=========+======+=============================+===================+======+=============+===========+===============+===========+==============+===========+================+===============================+
|    0x00 - 0x07    | 0x08 |   0x09  | 0x0A |            0x0B             |       0x0C        | 0x0D |     0x0E    | 0x0F-0x1F |      0x20     | 0x21-0x34 |     0x35     | 0x36-0x3B |    0x3C-0x3D   |           0x3E-0x3F           |
+-------------------+------+---------+------+-----------------------------+-------------------+------+-------------+-----------+---------------+-----------+--------------+-----------+----------------+-------------------------------+
| next/prev pointer | 0x09 |   0x0B  | 0x00 | Index of Room Property      | 0: Init           |      | Ignores ROM |           | Music before  |           | If nonzero,  |           | If nonzero,    | Set after fight is completed. |
| (used internally) |      |         |      | with pointer to object list | 1: Wait for start |      | value       |           | and after     |           | don't change |           | wait until set | If set during init,           |
|                   |      |         |      | to be loaded                | 2: Fight Ongoing  |      |             |           | the fight     |           | Music        |           | to start fight | don't start a fight.          |
+-------------------+------+---------+------+-----------------------------+-------------------+------+-------------+-----------+---------------+-----------+--------------+-----------+----------------+-------------------------------+

Monitor
-------

A number of monitors is created by the Controller when loading a fight, each keeping track of up to 8 created enemies. Once all of the enemies it is monitoring have died, it decreases it's parent Controller's Counter and deletes itself.

+-------------------+------+---------+------+-----------+--------+-----------+------------+-----------+-----------+
|                   | Type | Subtype | Form |           | Action |           | Parent     |           |           |
+===================+======+=========+======+===========+========+===========+============+===========+===========+
|    0x00 - 0x07    | 0x08 |   0x09  | 0x0A |    0x0B   |  0x0C  | 0x0D-0x13 |  0x14-0x17 | 0x18-0x1F | 0x20-0x3F |
+-------------------+------+---------+------+-----------+--------+-----------+------------+-----------+-----------+
| next/prev pointer | 0x09 |   0x0B  | 0x01 | Ignored   |        | Ignored   | Pointer    | Ignored   | Monitored |
| (used internally) |      |         |      |           |        |           | to         |           | Enemy     |
|                   |      |         |      |           |        |           | Controller |           | Pointers  |
+-------------------+------+---------+------+-----------+--------+-----------+------------+-----------+-----------+

Manager 4
=========

This :doc:`Manager </manager>` is responsible for digging cave entrances.

+-------------------+------+---------+-----------+-----------+-----------+
|                   | Type | Subtype |           |    Action |           |
+===================+======+=========+===========+===========+===========+
|    0x00 - 0x07    | 0x08 |   0x09  | 0x0A-0x0B |    0x0C   | 0x0D-0x3F |
+-------------------+------+---------+-----------+-----------+-----------+
| next/prev pointer | 0x09 |   0x04  | Ignored   | See below | Ignored   |
| (used internally) |      |         |           |           |           |
+-------------------+------+---------+-----------+-----------+-----------+

The data for digging cave entrances is a list per area, found from a table index by the area ID (NA: 0x08107DC0, EU: ???).

The structure for the digging cave entrance data is as follows:

+-------------+----------------+------+----------------+----------------+-------------+
| ?           | Source Room ID | ?    | Target Area ID | Target Room ID | ?           |
+======+======+================+======+================+================+======+======+
| 0x00 | 0x01 | 0x02           | 0x03 | 0x04           | 0x05           | 0x06 | 0x07 |
+------+------+----------------+------+----------------+----------------+------+------+

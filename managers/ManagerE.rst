Manager E
=========

This :doc:`Manager </manager>` waits for a flag to be set, then, if **? Flag** is set, does *something*. Then it waits for **Delay** frames, plays the SFX with ID **Sound ID** and loads the entity list at the room property indexed by **Entity List Prop**.
If the flag at **Flag ID** is set already when the manager is loaded, it does nothing.

+------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
|            Object Header           |                                                                                                                                     |
+-------------------+------+---------+------------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+-----------+-----------+
|                   | Type | Subtype | Entity List Prop | ? Flag | Action |      |                 |           |           | Sound ID  | Delay     |           | Flag ID   |
+===================+======+=========+==================+========+========+======+=================+===========+===========+===========+===========+===========+===========+
|                   | Loaded From ROM                            |               | Loaded From ROM |           |             Loaded From ROM                               |
+-------------------+------+---------+------------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+-----------+-----------+
|    0x00 - 0x07    | 0x08 |  0x09   |  0x0A            |  0x0B  |  0x0C  | 0x0D | 0x0E            | 0x0F-0x2F | 0x30-0x37 | 0x38-0x39 | 0x3a-0x3b | 0x3c-0x3d | 0x3e-0x3f |
+-------------------+------+---------+------------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+-----------+-----------+
| next/prev pointer | 0x09 |  0x0E   |                  |        |        |      |                 |           |           |           |           |           |           |
| (used internally) |      |         |                  |        |        |      |                 |           |           |           |           |           |           |
+-------------------+------+---------+------------------+--------+--------+------+-----------------+-----------+-----------+-----------+-----------+-----------+-----------+

Manager 6
=========

This :doc:`Manager </manager>` handles some special warps, like beanstalks, minish holes or the entrances into minish village.

+-------------------+------+---------+----------------+---------+-----------+---------+-----------------+-----------+---------------+-----------------------------+
|                   | Type | Subtype |  Entrance Prop |         |   Action  |         | ? Flag          |           | Entrance List |                             |
+===================+======+=========+================+=========+===========+=========+=================+===========+===============+===========+=================+
|                   | Loaded from ROM                           |                     | Loaded from ROM |                                       | Loaded from ROM |
+-------------------+------+---------+----------------+---------+-----------+---------+-----------------+-----------+---------------+-----------+-----------------+
|    0x00 - 0x07    | 0x08 |   0x09  |      0x0A      |  0x0B   |    0x0C   |   0x0D  |   0x0E          | 0x0F-0x1F |   0x20-0x23   | 0x24-0x2F |   0x30 - 0x3F   |
+-------------------+------+---------+----------------+---------+-----------+---------+-----------------+-----------+---------------+-----------+-----------------+
| next/prev pointer | 0x09 |   0x06  | Property Index | Ignored | See below | Ignored | Unknown         | Ignored   | See below     | Ignored                     |
| (used internally) |      |         | for Entrances  |         |           |         |                 |           |               |                             |
+-------------------+------+---------+----------------+---------+-----------+---------+-----------------+-----------+---------------+-----------+-----------------+

:Entrance Prop: The index of the current room property that is a pointer to the 0xFFFF-terminated list of `Manager 6 Entrance Data`_ structs.
:Entrance List: The pointer loaded from that room property.
:Action: Used to only load Entrance List once.
:? Flag: If set, the manager only works if the player state's byte at offset 0x12 is 0x1E.


Manager 6 Entrance Data
-----------------------

Each possible warp is encoded in the following structure:

+-------------------+-------------------+-----------------+-----------------+-----------------+--------+--------+----------+--------+
| Warp's x-Position | Warp's y-Position | Warp's x-Radius | Warp's y-Radius | Transition Prop |  Layer | Filler | ? Flag 2 | Filler |
+===================+===================+=================+=================+=================+========+========+==========+========+
|     0x00-0x01     |     0x02-0x03     |       0x04      |       0x05      |       0x06      |                0x07                 |
+-------------------+-------------------+-----------------+-----------------+-----------------+--------+--------+----------+--------+
|                   |                   |                 |                 |                 | 2 bits | 2 bits |   1 bit  | 3 bits |
+-------------------+-------------------+-----------------+-----------------+-----------------+--------+--------+----------+--------+

The corresponding warp gets triggered if the player's collision layer has a common bit with Layer, the player is at the position +- the radius of the corresponding coordinate, ? Flag 2 or the high bit of the byte at offset 0x30 of the player state is set, and the high byte of the Height property of the player entity is 0.

If all of the above are satisfied for one warp, the Transition at the room property indexed by Transition Prop is executed.

Manager 28
==========

This :doc:`Manager </manager>` creates some interaction between enemies.
From observation, it seems to make darknuts not overlap when there are multiple in a room, but it might do other things.

During initialization, it obtains pointers to all enemies loaded from a specified list that come before any manager 28 (typically, it would itself be in said list and obtain pointers to all enemies loaded before itself).
It then moves itself to list 6.

On each frame, for each pair of those enemies that still exist, it calls the function at 0x08004484 (NA address) with both of them as arguments, switching the order between calls.

If there is at most one enemy remaining, this manager will delete itself.


+------------------------------------+----------------------------------------------------------------------------------------------------+
| Object Header                      |                                                                                                    |
+-------------------+------+---------+----------+-----------+--------+---------+-------------------+--------------+-----------+-----------+
|                   | Type | Subtype | Form     |           | Action |         | Number of enemies | Counter      |           |           |
+===================+======+=========+==========+===========+========+=========+===================+==============+===========+===========+
|                   | Loaded from ROM                       |                                                                             |
+-------------------+------+---------+----------+-----------+--------+---------+-------------------+--------------+-----------+-----------+
|    0x00 - 0x07    | 0x08 |  0x09   | 0x0A     |    0x0B   |  0x0C  | 0x0D    |  0x0E             | 0x0F         | 0x10-0x1F | 0x20-0x3F |
+-------------------+------+---------+----------+-----------+--------+---------+-------------------+--------------+-----------+-----------+
| next/prev pointer | 0x09 |  0x028  | 0x01     | Room prop |        | Ignored | Decreases when    | Increases    | Ignored   | Monitored |
| (used internally) |      |         | (when in | for       |        |         | enemies die       | to alternate |           | Enemy     |
|                   |      |         | list 6)  | enemies   |        |         |                   | call order   |           | Pointers  |
+-------------------+------+---------+----------+-----------+--------+---------+-------------------+--------------+-----------+-----------+

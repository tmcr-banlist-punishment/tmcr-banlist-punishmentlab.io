Manager 5
=========

This :doc:`Manager </manager>` controls the spawn / removal of bridges, using **BridgeParams** to set the retractability, orientation and length of the bridge.

When the Flag at **Flag ID** is set, this manager will replace whatever tiles are present from 1 tile away in the direction of the bridge from the **X&Y tile position** to the end of it
with vertical (0x36) or horizontal (0x37) bridge tiles. If the bridge can rectract, when the flag at **Flag ID** gets unset, those previously overwritten tiles are replaced by "hole" tiles (0x323).

This behavior can be inverted using the **Invert** parameter, but the manager isn't going to spawn any bridge if the room is loaded with the flag not set.


+------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
|            Object Header           |                                                                                                                                           |
+-------------------+------+---------+------+--------------+--------+------+-----------------+-----------+-----------+-----------------+-----------------+-----------+-----------+
|                   | Type | Subtype |      | BridgeParams |        |      |       Invert    |           |           | X Tile position | Y tile position |           | Flag ID   |
+===================+======+=========+======+==============+========+======+=================+===========+===========+=================+=================+===========+===========+
|                   | Loaded From ROM                      |               | Loaded From ROM |           |             Loaded From ROM                                           |
+-------------------+------+---------+------+--------------+--------+------+-----------------+-----------+-----------+-----------------+-----------------+-----------+-----------+
|    0x00 - 0x07    | 0x08 |  0x09   | 0x0A |  0x0B        |  0x0C  | 0x0D | 0x0E            | 0x0F-0x2F | 0x30-0x37 | 0x38-0x39       | 0x3a-0x3b       | 0x3c-0x3d | 0x3e-0x3f |
+-------------------+------+---------+------+--------------+--------+------+-----------------+-----------+-----------+-----------------+-----------------+-----------+-----------+
| next/prev pointer | 0x09 |  0x0E   |      |              |        |      |                 |           |           |                 |                 |           |           |
| (used internally) |      |         |      |              |        |      |                 |           |           |                 |                 |           |           |
+-------------------+------+---------+------+--------------+--------+------+-----------------+-----------+-----------+-----------------+-----------------+-----------+-----------+

**BridgeParams** ( 1 Byte )

+----------------+----------------+-------------------------------------------------------------------+---------------------------------+
| Retractability |                |                               Length                              |            Orientation          |
+================+================+================+================+================+================+================+================+
|       X        |       X        |       X        |       X        |       X        |       X        |       X        |       X        |
+----------------+----------------+----------------+----------------+----------------+----------------+----------------+----------------+

*Orientation*

+---+---+----------+
| 0 | 0 |    Up    |
+---+---+----------+
| 0 | 1 |   Left   |
+---+---+----------+
| 1 | 0 |   Down   |
+---+---+----------+
| 1 | 1 |   Right  |
+---+---+----------+

Gitlab Pages Project for the TMCR Banlist Punishment Design Team

This is intended as a resources for tmc romhacking.

Pages are build from [reStructuredText] using [Sphinx].

To build the html pages locally, [install sphinx][sphinx], then run `make`.

[reStructuredText]: https://docutils.sourceforge.io/rst.html
[sphinx]: http://www.sphinx-doc.org/

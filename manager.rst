Managers
========

Managers (called "Events" in Minishmaker_), are a lot like :doc:`object`, in that they have associated code that runs every frame they are loaded.
However, they are missing some features common to other objects, to have a lower memory footprint.
For example, managers don't have sprites associated with them.

.. _Minishmaker: https://docs.minishmaker.com/

.. toctree::
   :maxdepth: 2
   :caption: Documented Managers

   managers/Manager4
   managers/Manager5
   managers/Manager6
   managers/ManagerB
   managers/ManagerE
   managers/Manager1E
   managers/Manager20
   managers/Manager28
   managers/Manager30

Each Manager occupies 0x40 bytes of memory:

+------------------------------------+-----------------------------------------------------------------------------+
|            Object Header           |                                                                             |
+-------------------+------+---------+--------+--------+-----------+-----------------+-----------+-----------------+
|                   | Type | Subtype | data-1 | data-2 |           |      data-3     |           |                 |
+===================+======+=========+========+========+===========+=================+===========+=================+
|                   | Loaded From ROM                  |           | Loaded From ROM |           | Loaded From ROM |
+-------------------+------+---------+--------+--------+-----------+-----------------+-----------+-----------------+
|    0x00 - 0x07    | 0x08 |   0x09  |  0x0A  |  0x0B  | 0x0C-0x0D |       0x0E      | 0x0F-0x2F |    0x30-0x3F    |
+-------------------+------+---------+--------+--------+-----------+-----------------+-----------+-----------------+
| next/prev pointer | 0x09 | Which   | General Purpose Data                                                        |
| (used internally) |      | Manager |                                                                             |
+-------------------+------+---------+-----------------------------------------------------------------------------+

In ROM, the initial data from each Manager is stored as 0x10 bytes:

+------+-----------+---------+--------+--------+--------+--------+--------+-------------+-------------+-------------+-------------+
| Type | Parameter | Subtype | data-1 | data-2 | data-3 | data-4 | data-5 | x           | y           | Flag 1      | Flag 2      |
+======+===========+=========+========+========+========+========+========+======+======+======+======+======+======+======+======+
| 0x00 |   0x01    |   0x02  |  0x03  |  0x04  |  0x05  |  0x06  |  0x07  | 0x08 | 0x09 | 0x0A | 0x0B | 0x0C | 0x0D | 0x0E | 0x0F |
+------+-----------+---------+--------+--------+--------+--------+--------+------+------+------+------+------+------+------+------+

Only the lower nibble of the Type byte is used for the type. Besides the entries corresponding to headings above, the entire ROM data is copied to the end of the struct in RAM (to the 0x30-0x3F area).

Tile Entities
=============

Tile entites are even more light-weight than managers.
They add simple behaviors to some tiles, like saving the state of bombable rocks or small trees, giving text to signs or determining items for chests.

In ROM, each tile entity has 8 bytes of data, the first being the type and the rest depending on said type.

.. toctree::
   :maxdepth: 2
   :caption: Documented Tile Entites

   tileEntities/tileEntity5
   tileEntities/tileEntity9
   tileEntities/tileEntityA
   tileEntities/tileEntityC

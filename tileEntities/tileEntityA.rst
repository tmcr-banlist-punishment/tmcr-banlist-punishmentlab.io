Tile Entity A
=============


This tile entity allows saving the state of bombable rocks, cut down small trees and the like.

+--------+---------+---------+--------------+--------------+---------+---------+
| Type   | Unknown | Flag    | X-Coordinate | Y-Coordinate | Unused? | Unknown |
+--------+---------+---------+--------------+--------------+---------+---------+
| 1 Byte | 1 Byte  | 2 Bytes | 6 bit        | 6 bit        | 4 bit   | 2 Byte  |
+--------+---------+---------+--------------+--------------+---------+---------+
|   0x0A |         |         |              |              |         |         |
+--------+---------+---------+--------------+--------------+---------+---------+

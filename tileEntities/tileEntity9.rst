Tile Entity 9
=============


This tile entity tells the game that this is a dark room.

Effectively, having this in a room:
  - Adds a halo of light around Link
  - Makes the UI being drawn on top of everything else

This doesn't effectively make the room dark, for that, there need to be a 0xBC object present as well.

Some rooms doesn't seem to support that well, maybe because they already have a background effect? (like the clouds' shadows in Hyrule Field)

+--------+---------+
| Type   | Unused? |
+--------+---------+
| 1 Byte | 7 Bytes |
+--------+---------+
|   0x09 |         |
+--------+---------+

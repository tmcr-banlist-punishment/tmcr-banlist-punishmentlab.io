Tile Entity C
=============


This tile entity displays the name at the offset, in blue when entering the room.

+--------+---------+----------+
| Type   | Offset  | Unused ? |
+--------+---------+----------+
| 1 Byte | 1 Byte  | 6 Bytes  |
+--------+---------+----------+
|   0x0C |         |          |
+--------+---------+----------+

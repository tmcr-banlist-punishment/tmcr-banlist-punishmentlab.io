Ennemy  Projectiles
===================

Ennemy projectiles are entities with the id 0x04.

The empty entries in that light may be hitboxes that are used by ennmies that don't use projectiles, as it seems that they instantiate these items from decomp.

.. list-table:: [WIP] Ennemy Projectiles
  :widths: 25 1000
  :header-rows: 1

  * - Object subID
    - Description
  * - 00
    -
  * - 01
    - Small octorock's roc
  * - 02
    - Stalfos's bone
  * - 03
    -
  * - 04
    - Deku seed
  * - 05
    -
  * - 06
    - Blue pesto ball
  * - 07
    - Green wizzrobe projectile (Type 1 - Red|Type 2 - Blue)
  * - 08
    - Fire wizzrobe projectile (Type 1)
  * - 09
    - Ice wizzrobe projectime (Type 1)
  * - 0A
    -
  * - 0B
    -
  * - 0C
    -
  * - 0D
    - Arrow
  * - 0E
    - Great Fairy electric projectile
  * - 0F
    - Big octo's rocs
  * - 10
    - Pot (for flying pots,I guess)
  * - 11
    - Cloud
  * - 12
    - V2/Lakitu electric ball
  * - 13
    -
  * - 14
    - Pink square (probably crenel's webs, cleared it with a Bomb or gust jar)
  * - 15
    - Madderpillar web
  * - 16
    - Lantern fireball
  * - 17
    -
  * - 18
    - V1 Dark orb
  * - 19
    -
  * - 1A
    -
  * - 1B
    - Canonnball
  * - 1C
    -
  * - 1D
    - Fire snake
  * - 1E
    - Rolling spikes
  * - 1F
    - V2 Purple spikes
  * - 20
    - V3 Hand projectiles
  * - 21
    -
  * - 22
    -
  * - 23
    -
  * - 24
    - V3 tennis ball
  * - 25
    -
  * - 26
    - Crashes the game
  * - 27
    - Crashes the game
  * - 28
    - Crashes the game
  * - 29
    - Crashes the game
  * - 2A
    - Crashes the game
  * - 2B
    - Invisible
  * - 2C
    - Invisible, spawns a broken tile
  * - 2D
    -
  * - 2E
    - Invisible, spawns broken tiles
  * - 2F
    - Invisible, spawns broken tiles
  * -
    - All projectiles beyond this point crashes the game, assuming the list ends here
  * - 30-3F
    - Crashes the game

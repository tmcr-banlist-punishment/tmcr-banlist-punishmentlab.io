Custom Music
============

This document outlines the process to replace the music in the game with user-supplied music.

It assumes some familiarity with event assembler, as used by both the randomizer and Minishmaker.

It does not provide information on how to edit or prepare midi files for use with the gba.

.. contents:: In this guide
   :depth: 1
   :local:

Tools
-----

The main tools needed for this process are mid2agb_ or `something equivalent`__ [#]_, and sappy2ea_ and the ``MPlayDef.event`` that comes with it.

.. _mid2agb: https://github.com/zeldaret/tmc/tree/master/tools/mid2agb
.. _midi2agb: https://github.com/ipatix/midi2agb
.. [#] I have not personally used this tool and make no guarantees about it's stability.
.. _sappy2ea: https://www.dropbox.com/s/nw5w6wqwny0wnrh/Sappy2EA.zip?dl=0

__ midi2agb_

Songs
-----

A song can be converted from a midi file to assembly [#]_ (a ``.s`` file) using mid2agb.
There are some restrictions on this, like a maximum number of channels, causing more channels in your midi file to be ignored.

.. [#] Really just a bunch of macro uses.

During this process, you will need to provide an index to a Voicegroup__, which gets expanded to a label like ``voicegroup_000`` (for index 000) that should point to the voicegroup.

__ Voicegroups_

By default, the filename of the midi file gets used for the label to be used in the `Sound Table`_.

.. _song pointer:

This label is *not* at the start of a song's data - songs as exported by mid2gba have the tracks' data first, followed by a header consisting of 4 bytes of various information (the first of which is the number of tracks), then the pointer to the voicegroup, and followed by a pointer for each track.

-----

Since this assembly file uses absolute pointers internally, we cannot simply convert it to a binary to be included in a custom patch.
Rather, we convert it to an ``.event`` using sappy2ea.
This file will include and use macro definitions from ``MPlayDef.event``, so make sure to put that into the same folder.

Sound Table
-----------

The sound table consists of 8-Byte wide entries, the first four of which are a pointer to `a song's header`__.
After that come two 2-Byte wide numbers.
We will leave these unchanged (they differentiate, for example, sound effects from music so that both can play at the same time).

__ `song pointer`_

The Sound Table in begins in the ROM at ``0xB1D414`` (EU address).

Below is a selection of it's entries (for a more complete table, that also includes sound effects, see `Team Minishmaker's Music Table Document`__).

.. _mmMusicTable: https://docs.google.com/spreadsheets/d/1eXff4Rx1LLbBgz1Jegp40T7jofUyYwmdxuVQiv_8iFw/edit

__ mmMusicTable_

===== ==================================
Index Song Name
===== ==================================
   3  Title Screen
   5  Element Get
   6  Fairy Fountain
   7  File Select
   9  Credits
  10  Game Over
  19  Climbing a Beanstalk
  20  House Theme
  21  Cucco Minigame
  22  Syrup's Theme
  23  Dungeon
  24  Element Theme/Wind Tribe Motif
  25  Hyrule Field
  26  Hyrule Castle
  27  Hyrule Castle (No Intro)
  28  Minish Village
  29  Minish Woods
  30  Crenel Storm
  31  Castor Wilds
  32  Hyrule Town
  33  Royal Valley
  34  Cloud Tops
  35  Dark Hyrule Castle
  36  Secret Castle Entrance
  37  Deepwood
  38  Cave of Flames
  39  Fortress of Winds
  40  Temple of Droplets
  41  Palace of Winds
  43  Royal Crypt
  44  Elemental Sanctuary
  45  Fight Theme
  46  Boss Theme
  47  Vaati Reborn
  48  Vaati Transfigured
  49  Castle Collapse
  50  Vaati's Wrath
  51  Fight Theme 2
  52  Digging Cave
  53  Dojo
  55  Mt. Crenel
  57  Lost Woods
  58  Fairy Fountain 2
  59  Wind Ruins
  94  Scroll Learnt
===== ==================================

Voicegroups
-----------

A voicegroup is the equivalent of a soundfont - it is a table providing instrument data.
These instruments are typically at the same indices as they would be in a `General Midi Soundfont`_
However, oftentimes instruments are going to be missing (the entry is just a square wave) if they do not get used by any song in the vanilla game.

.. _General Midi Soundfont: https://en.wikipedia.org/wiki/General_MIDI#Program_change_events

To avoid having to stick to the vanilla game's instruments, you can provide your own voicegroup(s).
We have done this for the pokemon custom music used in the `Fall 2020 Banlist Punishment Hack`_, based on how they were defined in the `Pokémon Emerald decomp`__.

.. _Fall 2020 Banlist Punishment Hack: http://bombch.us/DPuF

__ https://github.com/pret/pokeemerald

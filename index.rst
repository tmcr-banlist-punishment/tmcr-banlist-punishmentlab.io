TMC Romhacking Information and Resources
========================================

Overview
--------

This repository contains information about things we learned from making the Banlist Punishment Romhacks.
It is intended to serve as both an information repository of the workings in the game, with a focus on things that are useful in the production of romhacks, and a collection of guides on how to achieve specific, more complicated things.

.. toctree::
   :maxdepth: 2
   :caption: Information about the game's internals:

   ennemyProjectile.rst
   object.rst
   manager.rst
   tileEntity.rst
   transition.rst

.. toctree::
   :maxdepth: 1
   :caption: Guides

   customMusic.rst

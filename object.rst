Objects
=======

Here is a basic list of the objects in the game.

.. list-table:: [WIP] Objects
  :widths: 25 1000
  :header-rows: 1

  * - Object subID
    - Description
  * - 00
    - Freestanding item - Also used for ennemies drops
  * - 01
    - Ennemy death animation - May drop an item
  * - 02
    - Shop item
  * - 03
    - ??? (doesn't seem to do anything)
  * - 04
    - ??? (doesn't seem to do anything)
  * - 05
    - Pot
  * - 06
    - Ezlo ? (Despawns after half a second)
  * - 07
    - Block moving animation
  * - 08
    - Dungeon Door
  * - 09
    - ??? (doesn't seem to do anything)
  * - 0A
    - ??? (doesn't seem to do anything)
  * - 0B
    - ??? (doesn't seem to do anything)
  * - 0C
    - Chest spawner ?
  * - 0D
    - Unused "Skulls" - Two types exist, there doesn't seem to be any difference between them other than the sprite.
  * - 0E
    - Tile crumbling animation (doesn't change the tile it's on)
  * - 0F
    - Item/Link falling into a drop animation ?
  * - 10
    - Clone pad lighting animation - wierdly seems to leave a small hitbox
  * - 11
    - Bush ? (Despawns after half a second)
  * - 12
    - ToD big stone doors - No idea how to get them to open
  * - 13
    - Minecart track
  * - 14
    - Big Lilypad
  * - 15
    - Visual glitch ? (Similar to the beanstalk and sometimes the ToD madderpillars in rando)
  * - 16
    - Moving platefrom
  * - 17
    - Smoke puff ? (Despawns after half a second)
  * - 18
    - Gregal's ghost
  * - 19
    - Crashes the ROM
  * - 1A
    - ??? (looks like a small shadow moving in the room for a few seconds)
  * - 1B
    - Spawn Great fairy (Sprites not on the right order) (triggerd by flag 2)
  * - 1C
    - Hyrule Town Post roof decoration (no collision)
  * - 1D
    - ??? (doesn't seem to do anything)
  * - 1E
    - Dirt digging animation
  * - 1F
    - Same as 0x11, but 2-3 tiles higher than the object's coordinates (Maybe used when picking up bushes ?)
  * - 20
    - Bomb Explosion
  * - 21
    - Yellow puff animation
  * - 22
    - Carlov's machines (Type 0 is fig list, type 1 is egg machine (is triggered by any switch in the room), type 2 is egg drop cinematic)
  * - 23
    - Eye switch (only collides with arrows)
  * - 24
    - Unused big switch (gets activated when creating clones nearby ??)
  * - 25
    - DWS's barrel
  * - 26
    - ??? - decoration (maybe used for the floor during the minish transformation cutscenes ? )
  * - 27
    - Pushable statue
  * - 28
    - ??? (doesn't seem to do anything)
  * - 29
    - PoW clouds (no collison?)
  * - 2A
    - Flame animation (loops)
  * - 2B
    - ??? (1 dark pixel)
  * - 2C
    - Growing beanstalk animation
  * - 2D
    - Chimney smoke
  * - 2E
    - Pushable boulder
  * - 2F
    - ??? solid wierd looking tile (might be an unpushable block)
  * - 30
    - ??? (doesn't seem to do anything)
  * - 31
    - ToD floor decoration (frozen flower) - Doesn't change the tile type
  * - 32
    - Mushroom
  * - 33
    - Barrier / Water Barrier
  * - 34
    - Blue/Red/Green warp (triggered by flag 2)
  * - 35
    - ??? (Some kind a screen with a chest appearing on it ?) - Probably unused
  * - 36
    - ??? (doesn't seem to do anything)
  * - 37
    - ??? (doesn't seem to do anything)
  * - 38
    - Flippable minish pot (doesn't have collison, but can be flipped with cane)
  * - 39
    - Big key door
  * - 3A
    - Part of the element get animation (Triangle screen effect)
  * - 3B
    - Minish stump interior mushrooms bases ? (don't have collision)
  * - 3C
    - Link becoming minish animation
  * - 3D
    - ??? makes everything in the screen become black. Link will appear with its palette messed up if menu is opened/closes
  * - 3E
    - Minish village (seen as normal sized Link)
  * - 3F
    - Big Leaf (no collision)
  * - 40
    - Fairy
  * - 41
    - Ladder
  * - 42
    - ??? (doesn't seem to do anything)
  * - 43
    - ??? (doesn't seem to do anything)
  * - 44
    - ??? (doesn't seem to do anything)
  * - 45
    - ??? (doesn't seem to do anything)
  * - 46
    - Climbable book
  * - 47
    - Heart Container Spawn cinematic (spawns a null item by default) (triggered by flag2)
  * - 48
    - Filename selection hover (locked on file 1?)
  * - 49
    - ??? (doesn't seem to do anything)
  * - 4A
    - ??? (doesn't seem to do anything)
  * - 4B
    - Small green chu spawning animation
  * - 4C
    - Pushable bookshelf (See Manager 0x26)
  * - 4D
    - Unpushable funiture
  * - 4E
    - Town house minish entrance
  * - 4F
    - Doorframe
  * - 50
    - Big rock (Decoration) - No collison
  * - 51
    - (Unused ?) big rock with a collider larger than the sprite
  * - 52
    - ??? (doesn't seem to do anything)
  * - 53
    - Bush being flipped (with cane) animation
  * - 54
    - Handle
  * - 55
    - ??? (doesn't seem to do anything)
  * - 56
    - Exclamation point animation
  * - 57
    - ??? (doesn't seem to do anything)
  * - 58
    - Gentari's curtains
  * - 59
    - CoF lava plateform
  * - 5A
    - Piece of paper (no collion, cannot be read)
  * - 5B
    - Bottom part of Link's bed, when sleeping with Ezlo (no collision)
  * - 5C
    - Mask
  * - 5D
    - House door
  * - 5E
    - Small tornado
  * - 5F
    - Pushable school statue
  * - 60
    - Swordman's Newsletter (readable)
  * - 61
    - ??? (doesn't seem to do anything)
  * - 62
    - Big twig
  * - 63
    - ??? (doesn't seem to do anything)
  * - 64
    - lighting animation (before V3 fight, darknut spawns)
  * - 65
    - Some piece of wood ? (maybe a part of Minish rem's desk ?)
  * - 66
    - ??? (doesn't seem to do anything)
  * - 67
    - Plateform falling into lava animation
  * - 68
    - Splash animation
  * - 69
    - ??? (doesn't seem to do anything) (Linked to Great Fairies spawn)
  * - 6A
    - Big chest (no collison or interaction)
  * - 6B
    - beans (appears way right and up from the object) (seems like green is type 0 and blue type 2, big green bean is type 3 (for minish Link) )
  * - 6C
    - Another dungon door
  * - 6D
    - Pushable pillar
  * - 6E
    - ??? (doesn't seem to do anything)
  * - 6F
    - ??? (top layer sprite)
  * - 70
    - Swamp
  * - 71
    - Royal Crypt Tombstone
  * - 72
    - FoW big stone tablet (unreadable)
  * - 73
    - Small lilypad
  * - 74
    - Spawns a minsh portal (triggered by flag2) (doesn't spawn the manager linked to the portal ?)
  * - 75
    - ??? (decoration - no collison)
  * - 76
    - Big flower bottom (decoration - no collison)
  * - 77
    - Town Bell
  * - 78
    - Minish Lilypad flower (Turns tiles around it into water)
  * - 79
    - Visual spark (decoration - no collision)
  * - 7A
    - Vapor/smoke swirl (From CoF top)
  * - 7B
    - ToD small lever
  * - 7C
    - Big shoes
  * - 7D
    - Bush destroy animation
  * - 7E
    - ??? (doesn't seem to do anything)
  * - 7F
    - Picolyte Flower
  * - 80
    - Wood Board (unused ?)
  * - 81
    - Small bench
  * - 82
    - Big tornado warp (starts reading warps from ToD exit warp)
  * - 83
    - Big ToD lever
  * - 84
    - Ice block
  * - 85
    - Right side ToD ice chunck
  * - 86
    - Librari's clover trap door
  * - 87
    - Flame ? (Despawns after half a second)
  * - 88
    - Giant green book. (Has the animations, but cannot fall)
  * - 89
    - Mazaal Fight - Spawns Mazaal's main body, which dies instantly, softlocking the player and confusing the camera
  * - 8A
    - Furnace
  * - 8B
    - Juliette's bookshelf
  * - 8C
    - ??? (doesn't seem to do anything)
  * - 8D
    - Fireplace (Can be extinguished using water)
  * - 8E
    - ??? (Makes the whole screen dark)
  * - 8F
    - Frozen element/object
  * - 90
    - Question mark (probably rando only)
  * - 91
    - ??? (doesn't seem to do anything)
  * - 92
    - Bakery furnace
  * - 93
    - Small Lamp (need to test if it draws a light halo when placed in dark room)
  * - 94
    - Wind tribe sign
  * - 95
    - Zeffa dropping ocarina/item (actually drops the item) Other types are static birds in PoW entrance
  * - 96
    - Items obtained by bonking a tree
  * - 97
    - ??? (doesn't seem to do anything)
  * - 98
    - Fire bar
  * - 99
    - ??? (doesn't seem to do anything)
  * - 9A
    - Giant acorn
  * - 9B
    - ??? (doesn't seem to do anything)
  * - 9C
    - Pegasus tree top
  * - 9D
    - Crystal switch
  * - 9E
    - Tree thorns (only damages link from the bottom)
  * - 9F
    - Small wind turbine (doesn't pushes link)
  * - A0
    - Canon
  * - A1
    - entire doorframe (seems like from PoW) ?
  * - A2
    - ??? (plays the figurine egg dropping sfx)
  * - A3
    - CT big tornado spawn animation
  * - A4
    - Glowing mushroom in a pot (no collision)
  * - A5
    - Fire snake
  * - A6
    - ??? (doesn't seem to do anything)
  * - A7
    - Unused ? (green square with four dots circling around a fifth one)
  * - A8
    - Null item ? (doesn't display, but can be picked up)
  * - A9
    - Smoke puff
  * - AA
    - Open waterfall tile (no collision)
  * - AB
    - Opens V1 Dropdown (Only visual) (triggered by flag 2)
  * - AC
    - Element get cutscene (doesn't seem to have any item tied to it by default, for the earth element by default)
  * - AD
    - ??? (1 dark pixel)
  * - AE
    - block (no collision)
  * - AF
    - ??? (doesn't seem to do anything)
  * - B0
    - Gauntlet entrance door
  * - B1
    - DHC cells
  * - B2
    - wind? (Despawns after half a second)
  * - B3
    - Kinstone fusion spark
  * - B4
    - Japanse subtitle (ふしぎのぼうし) with animated Ezlo judging you
  * - B5
    - ??? (doesn't seem to do anything)
  * - B6
    - ??? (doesn't seem to do anything)
  * - B7
    - Invisible collider
  * - B8
    - Fusion warp
  * - B9
    - Cucoo Game timer (at 55.0 by default. Doesn't start)
  * - BA
    - Spawns red gyorg (but no blue one, so you can't clear the fight); Breaks a lot of stuff
  * - BB
    - Wind crest stone (no collision)
  * - BC
    - Light Halo for dark rooms (puts room in the dark, lantern doens't work on this dark)
  * - BD
    - The legend of Zelda : The minish cap title screen(also contains part of the sword animated)
  * - BE
    - Small windmill (no collision)
  * - BF
    - ??? (doesn't seem to do anything)
  * - ===
    - ===Not sure these are still valid items beyond this point===
  * - C0
    - ??? (doesn't seem to do anything)
  * - C1
    - Link getting an item animation (turns Link invisible for a few seconds)
  * - C2
    - ??? Crashes
  * - C3
    - ??? Crashes
  * - C4
    - turns top layer invisible
  * - C5
    - ??? (doesn't seem to do anything)
  * - C6
    - ??? (doesn't seem to do anything)
  * - C7
    - ??? Crashes
  * - C8
    - ??? (doesn't seem to do anything)
  * - C9
    - ??? Crashes
  * - CA
    - ??? (doesn't seem to do anything)
  * - CB
    - ??? (doesn't seem to do anything)
  * - CC
    - ??? (doesn't seem to do anything)
  * - CD
    - ??? (doesn't seem to do anything)
  * - CE
    - ??? Crashes
  * - CF
    - ??? Crashes
  * - D0
    - ??? (doesn't seem to do anything)
  * - D1
    - ??? (doesn't seem to do anything)
  * - D2
    - Destroys palette/tileset
  * - D3
    - ??? (doesn't seem to do anything)
  * - D4
    - Destroys palette/tileset
  * - D5
    - ??? (doesn't seem to do anything)
  * - D6
    - ??? (doesn't seem to do anything)
  * - D7
    - Spawns a wierd halo (messes with room lighting)
  * - D8
    - ??? (doesn't seem to do anything)
  * - D9
    - ??? (doesn't seem to do anything)
  * - DA
    - ??? (doesn't seem to do anything)
  * - DB
    - Broken sprite
  * - DC
    - ??? (doesn't seem to do anything)
  * - DD
    - ??? (doesn't seem to do anything)
  * - DE
    - minish rain animation (doesn't do damage)
  * - DF
    - ??? (doesn't seem to do anything)
  * - E0
    - ??? (doesn't seem to do anything)
  * - E1
    - More broken sprites
  * - E2
    - ??? (doesn't seem to do anything)
  * - E3
    - ??? Crashes
  * - E4
    - ??? (doesn't seem to do anything)
  * - E5
    - ??? (doesn't seem to do anything)
  * - E6
    - ??? Crashes
  * - E7
    - ??? Crashes
  * - E8
    - ??? (doesn't seem to do anything)
  * - E9
    - ??? (doesn't seem to do anything)
  * - EA
    - ??? (doesn't seem to do anything)
  * - EB
    - ??? (doesn't seem to do anything)
  * - EC
    - ??? (doesn't seem to do anything)
  * - ED
    - Loads cannons and other sprites ?
  * - EE
    - ??? (doesn't seem to do anything)
  * - EF
    - ??? (doesn't seem to do anything)
  * - F0
    - ??? (doesn't seem to do anything)
  * - F1
    - ??? (doesn't seem to do anything)
  * - F2
    - ??? (doesn't seem to do anything)
  * - F3
    - ??? (doesn't seem to do anything)
  * - F4
    - Vaati TP animation (but doesn't spawn any sprite other than red lines by default)
  * - F5
    - ??? (doesn't seem to do anything)
  * - F6
    - ??? (doesn't seem to do anything)
  * - F7
    - ??? (doesn't seem to do anything)
  * - F8
    - ??? (doesn't seem to do anything)
  * - F9
    - ??? (doesn't seem to do anything)
  * - FA
    - ??? (doesn't seem to do anything)
  * - FB
    - ??? Crashes
  * - FC
    - ??? (doesn't seem to do anything)
  * - FD
    - ??? Crashes
  * - FE
    - ??? Crashes
  * - FF
    - Same as 0x15
